#!/usr/bin/env python

from twisted.internet import protocol, reactor

HOST='localhost'
PORT=21567

class TSClntProtocol(protocol.Protocol):
    def sendData(self):
        data = input('clnt_input>>>')
        if data:
            print('...sending %s...' %data)
            self.transport.write(data.encode('UTF-8'))
        else:
            self.transport.loseConnection()

    def connectionMade(self):
        self.sendData()

    def dataRecieved(self, data):
        print('have received data')
        print(data.decode('UTF-8'))
        self.sendData()


class TSClntFactory(protocol.ClientFactory):
    protocol = TSClntProtocol
    clientConnectionLost = clientConnectionFailed = \
                           lambda self, connector, reason: reactor.stop()

reactor.connectTCP(HOST, PORT, TSClntFactory())
reactor.run()
