import math

def userSimilarity(train):
    # build inverse table for items_users
    item_users=dict()
    for u,items in train.items():
        for i in items:
            if i not in item_users:
                item_users[i]=set()
            item_users[i].add(u)

    #calculate co-rated items between users
    C=dict()
    N=dict()
    for i,users in item_users.items():
        for u in users:
            if u in N.keys():
                N[u] += 1
            else:
                N[u] = 1

            for v in users:
                if u==v:
                    continue
                if (u,v) in C:
                    C[u,v] += 1
                else:
                    C[u,v] = 1
    #print(C)
   #calculate final similarity matrix w
    w=dict()
    for u, value in C.items():
       a = u[0]
       b = u[1]
       temp = {}
       temp[b] = value / math.sqrt(N[a] * N[b])
       if a in w.keys():
           combine = temp.copy()
           combine.update(w[a])
           w[a] = combine
           #w[a] = dict(w[a].items() + temp.items())
       else:
           w[a] = temp
    print(w)
    return w

def recommend(user, train, w):
    rank = dict()
    interacted_items = train[user]
    for v, wuv in sorted(w[u].items, key=itemgetter(1), reverse=True)[0,K]:
        for i, rvi in train[v].items:
            if i in interacted_items:
                #we should filter items user interacted before
                continue
            rank[i] += wuv + rvi
    return rank


#main function
sdm_file = open("sdm.txt")
train_dict = {}
K = 10
#discard the first line
sdm_file.readline()
for line in sdm_file.readlines():
    line = line.strip()
    uid, bid = line.split("\t")
    bid = bid.strip('\n')
    bid = bid.split(',')
    if uid in train_dict:
        train_dict[uid].add(bid[0])
    else:
        train_dict[uid] = set(bid)
#now train_dict is in the right format
#print(train_dict)

#call userSimilarity function
userSimilarity(train_dict)
