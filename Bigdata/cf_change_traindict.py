import math
from operator import itemgetter

def userSimilarity(train):
    # build inverse table for items_users
    item_users = dict()
    for u, items in train.items():
        for i in items.keys():
            if i not in item_users:
                item_users[i] = set()
            item_users[i].add(u)

    #print item_users
    for b, u in item_users.items():
        print("item_user[%s] = " %b, end=' ')
        print(u)

    #calculate co-rated items between users
    C = dict()
    N = dict()
    for i, users in item_users.items():
        for u in users:
            if u in N.keys():
                N[u] += 1
            else:
                N[u] = 1

            for v in users:
                if u == v:
                    continue
                if (u, v) in C:
                    C[u, v] += 1
                else:
                    C[u, v] = 1
    #print(N)
    #print(C)
    #calculate final similarity matrix w
    w = dict()
    for u, value in C.items():
       a = u[0]
       b = u[1]
       temp = {}
       temp[b] = value / math.sqrt(N[a] * N[b])
       if a in w.keys():
           combine = temp.copy()
           combine.update(w[a])
           w[a] = combine
           #w[a] = dict(w[a].items() + temp.items())
       else:
           w[a] = temp
    #print(w)
    return w

#integrated our own save function
def recommend(user, train, w, result_file):
    rank = dict()
    interacted_items = train[user]
    if user not in w.keys():
        return
    s_temp = sorted(w[user].items(), key=itemgetter(1), reverse=True)
    for v, wuv in s_temp[:K]:
        for i, rvi in train[v].items():
            if i in interacted_items:
                #we should filter items user interacted before
                continue
            if i in rank:
                rank[i] += wuv * rvi
            else:
                rank[i] = wuv * rvi
            result_file.write(str(user) + ',' + str(i) + ',' + str(v) + ',' + str(wuv) + ',' + str(rank[i]) + '\n')
    return rank


#main function
sdm_file = open("sdm.txt")
train_dict = {}
#top K similar users
K = 3
#rvb: user v's score against brand b
default_rvb = 1.0
#discard the first line
sdm_file.readline()
for line in sdm_file.readlines():
    line = line.strip()
    uid, bid = line.split("\t")
    bid = bid.strip('\n')
    bid = bid.split(',')[0] #we have only one brand each line
    temp = {}
    if uid in train_dict:
        temp[bid] = default_rvb
        combine = temp.copy()
        combine.update(train_dict[uid])
        train_dict[uid] = combine
    else:
        train_dict[uid] = {bid: default_rvb}
#now train_dict is in the right format
#print(train_dict)

#call userSimilarity function
w = userSimilarity(train_dict)
#write the result into file
cf_result_file = open("cf_result.txt", "w")
#userid, brandid,相似的用户，两用户的相似系数，用户对brand的兴趣分值
cf_result_file.write('user_id' + ',' + 'brand_id' + ',' + 'similar_user_id' + ',' + 'similar_weight' + ',' + 'brand_grade' + '\n')
for u, items in train_dict.items():
    recommend(u, train_dict, w, cf_result_file)

cf_result_file.close()


