from collections import defaultdict

#main function
predict_num = 0
hit_num = 0
bought_brand = 0

result = defaultdict(set)
result_file = open("result.txt")
for line in result_file.readlines():
    line = line.strip()
    uid, bid = line.split("\t")
    result[uid] = bid.split(',')
    bought_brand += len(result[uid])
result_file.close()

predict_file = open("predict.txt")
for line in predict_file.readlines():
    line = line.strip()
    uid, bid = line.split("\t")
    bid = bid.strip('\n')
    bid = bid.split(",")
    predict_num += len(bid)
    if uid not in result:
        continue
    else:
        for i in bid:
            if i in result[uid]:
                hit_num += 1

precision = float(hit_num) / predict_num
callrate = float(hit_num) / bought_brand
f1 = 2 * precision * callrate / (precision + callrate)
