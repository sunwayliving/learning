from datetime import *

def parse_date(raw_date):
    entry_date = raw_date.decode("GBK")
    month = int(entry_date[0])
    if len(entry_date[0]) == 5:
        day = 10 * int(entry_date[2]) + int(entry_date[3])
    else:
        day = int(entry_date[2])

    return 2013, month, day

def generate_train_set(raw_file_name, begin_date, end_date):
    raw_file = open(raw_file_name)
    train_set_name = str(begin_date.month) + "_" + str(end_date.month) + "_train.csv"
    train = open(train_set_name, "w")
    raw_file.readline()
    for line in raw_file.readlines():
        line = line.strip()
        entry = line.split(",")
        entry_date = date(*parse_date(entry[3]))
        date_delta = (entry_date - begin_date).days
        if entry_date >= begin_date and entry_date <= end_date:
            train.write(",".join(entry[:3]) + "," + str(date_delta) + "\n")
    train.close()
    raw_file.close()

#we only use buy record
def generate_validation_set(raw_file_name, begin_date, end_date):
    raw_file = open(raw_file_name)
    validation_set_name = str(begin_date.month) + "_" + str(end_date.month) + "_validation.csv"
    validation = open(validation_set_name, "w")
    raw_file.readline()
    for line in raw_file.readlines():
        line = line.strip()
        entry = line.split(",")
        entry_date = date(*parse_date(entry[3]))
        if entry_date >= begin_date and entry_date <= end_date and int(entry[2]) == 1:
            validation.write(",".join(entry[:2]) + "\n")
    #validation.write("99999999999,9" + "\n")
    validation.close()
    raw_file.close()

#file format: user_id,brand_id
#1. delete duplicate record
#2. format the result according to the given format
def generate_result(file_name):
    file = open(file_name)
    entrys = file.readlines()
    entrys.sort(key = lambda x: x.split(",")[0])
    result = open("result.txt", "w")
    for index, entry in enumerate(entrys):
        uid, bid = entry.strip().split(",")
        if index == 0:
            cur_id = uid
            cur_result = [bid]
            continue

        if uid != cur_id:
            result.write(cur_id + "\t" + ",".join(set(cur_result)) + "\n")
            cur_id = uid
            cur_result = [bid]
        if index == len(entrys) - 1:
            result.write(cur_id + "\t" + ",".join(set(cur_result)) + "\n")

        cur_result.append(bid)
    file.close()


#main function begins
train_begin_date = date(2013, 5, 15)
train_end_date = date(2013, 7, 15)
generate_train_set("t_alibaba_data.csv", train_begin_date, train_end_date)

validation_begin_date = date(2013, 7, 15)
validation_end_date = date(2013, 8, 15)
generate_validation_set("t_alibaba_data.csv", validation_begin_date, validation_end_date)

generate_result("7_8_validation.csv")
