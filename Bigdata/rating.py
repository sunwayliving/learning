import math
from operator import itemgetter

class Record(object):
    pass

#records 用户对brand的评分记录
#item_users = {item_id_1:{user_id_1 : value1, user_id_2 : value2}, item_id_2:{....}}
def UserSimilarity(records):
    item_users = dict()
    ave_vote = dict()
    activity = dict()
    temp = dict()
    for r in records:
        #addToMat(item_users,r.item,r.user,r.value)
        if r.item in item_users.keys():
            temp[r.user] = r.value
            combine = temp.copy()
            combine.update(item_users[r.item])
            item_users[r.item] = combine
        else:
            item_users[r.item] = {r.user: r.value}

        #addToVec(ave_vote,r.user,r.value)
        if r.user in ave_vote.keys():
            ave_vote[r.user] += float(r.value)
        else:
            ave_vote[r.user] = float(r.value)

        #addToVec(activity,r.user,1)
        if r.user in activity:
            activity[r.user] += 1
        else:
            activity[r.user] = 1
    for x, y in ave_vote.items():
        ave_vote[x] = float(y) / float(activity[x])
    #ave_vote = {x: float(y) / float(activity[x]) for x, y in ave_vote.items()}

    nu = dict()
    w = dict()
    for i, ri in item_users.items():
        for u, rui in ri.items():
            #addToVec(nu,u,(rui-ave_vote[u])*(rui-ave_vote[u]))
            if u in nu.keys():
                nu[u] += (rui - ave_vote[u]) * (rui - ave_vote[u])
            else:
                nu[u] = (rui - ave_vote[u]) * (rui - ave_vote[u])

            for v, rvi in ri.items():
                if u == v:
                    continue
                #addToMat(w,u,v,(rui-ave_vote[u])*(rui-ave_vote[v]))
                if u in w.keys() and v in w[u].keys():
                    w[u][v] += (rui-ave_vote[u])*(rvi-ave_vote[v])
                else:
                    w[u] = {v: ((rui-ave_vote[u])*(rvi-ave_vote[v]))}
    #皮尔逊系数
    for u in w:
        for x, y in w[u].items():
            if nu[x] == 0 or nu[u] == 0:
                w[u] = {x: 0.0}
            else:
                w[u] = {x: y / math.sqrt(nu[x] * nu[u])}
        #w[u] = {x: y/math.sqrt(nu[x]*nu[u]) for x, y in w[u].items()}
    return w, ave_vote


#records 用户对brand的评分记录，test好像是测试集，ave_vote 用户对brand评分的平均值，
#     w 相似系数，k是选取的相似系数最大的用户数
#records: 训练集， test: 测试集
def PredictAll(records, test, ave_vote, w, k):
    user_items = dict()
    for r in records:
        #addtoMat(user_items,r.user,r.item,r.value)
        temp = dict()
        if r.user in user_items.keys():
            temp[r.item] = r.value
            combine = temp.copy()
            combine.update(user_items[r.user])
            user_items[r.user] = combine
        else:
            user_items[r.user] = {r.item: r.value}

    for r in test:
        r.predict = 0
        norm = 0
        if r.user in w.keys():
            s_w = sorted(w[r.user].items(), key=itemgetter(1), reverse=True)
            for v, wuv in s_w[:K]:
               if r.item in user_items[v]:
                    rvi = user_items[v][r.item]
                    r.predict += wuv * (rvi - ave_vote[v])
                    norm += abs(wuv)
            if norm > 0:
                r.predict /= norm
            r.predict += ave_vote[r.user]
        else:
            r.predict = -1 #预测的用户如果没有训练过的话,预测分数为-1



#main function
K = 5

#制作训练集
rating_record_file = open("ratingRecord.txt")
#records是训练集,是一个list; test 测试集
records = []
#discard the first line
rating_record_file.readline()
for line in rating_record_file.readlines():
    line = line.strip()
    user_id, item_id, credit = line.split('\t')
    r_tmp = Record()
    r_tmp.user = user_id
    r_tmp.item = item_id
    r_tmp.value = float(credit)
    records.append(r_tmp)
rating_record_file.close()

#制作测试集
rating_test_file = open("ratingTest.txt")
test = []
for line in rating_test_file.readlines():
    line = line.strip()
    user_id, item_id, credit = line.split('\t')
    r_tmp = Record()
    r_tmp.user = user_id
    r_tmp.item = item_id
    r_tmp.value = float(credit)
    test.append(r_tmp)
rating_test_file.close()
#for t in test:
#    print(t.user, t.item, t.value)
#计算用户相似度
w, ave_vote = UserSimilarity(records)

#利用训练出来的模型进行预测
PredictAll(records, test, ave_vote, w, K)
#保存预测结果
predict_result_file = open("ratingPredict.txt", "w")
predict_result_file.write("user_id" + "\t" + "brand_id" + "\t" + "Credit" + "\n")
for t in test:
    predict_result_file.write(t.user + "\t" + t.item + "\t" + str(t.predict) + "\n")
predict_result_file.close()